-- SUMMARY --

The Invideous module provides both utility functions to communicate with the Invideous system and a submodule
that will integrate the Ooyala module provided player with the Invideous paywall system.

-- REQUIREMENTS --

Requires an account with Invideous and a valid API keys.

Integration with Ooyala requires the Ooyala module:
http://www.drupal.org/project/ooyala

-- INSTALLATION --

* Install as usual, see http://www.drupal.org/documentation/install/modules-themes/modules-7 for more information.

-- CONFIGURATION --

* Configure user permissions in Administration >> People >> Permissions

  - Use Invideous
    All API requests require the base permission of using the Invideous API. An access exception error will be thrown
    and users will not be able to register or login into Invideous without the proper permissions.

  - Administer Invideous
    The administrator page for configuring the API keys requires this permission. Without it, a user
    will not be able to configure the API keys or see any API keys that have already been set. As well,
    users will not be able to configure available tariffs.

* Configure the Invideous API keys in Administration >> Configuration >> Services >> Invideous

  - The Secret Access Key is needed in order to perform any administrative write actions to Invideous.
    i.e. Registering new videos.

  - The Public Key is used in conjunction with the secret access key in order to perform write actions to
    Invideous.

  - The Publisher ID is used in a variety of API calls with Invideous in order to identify the account from
    which the call is being made. i.e. Getting packages or video details.

  - The Read Token is needed to perform any administrative read actions to Invideous.
    i.e. Getting sales information.

* Optionally configure the Invideous additional functionality settings in Administration >> Configuration >> Services >> Invideous

  - The Show Buy Now Button will immediately display a button in the top right hand corner of the flash player. This button
    will allow users to immediately bypass the paywall and buy the video before the paywall timer runs out. Currently this
    only works in conjunction with the Ooyala integration submodule.

  - The Enable Automatic Registration checkbox determines whether or not to automatically register new Drupal accounts with
    Invideous. This action happens upon first successful login.

  - The Enable SSO checkbox is dependant on automatic registration and determines whether or not single sign on is active.
    SSO will temporarily redirect users to the Invideous authentication server upon login and place a session id cookie
    on their browser. This allows videos that are viewed on the Drupal site to automatically detect Invideous users and accounts.
    Currently, SSO authentication only works with non-ajax login.

-- TROUBLESHOOTING --

* If the menu does not display, check the following:

  - Are the 'Administer Invideous' permissions set for the proper roles?

* If users are not being logged into Invideous automatically:

  - Is Invideous SSO enabled?
      Non SSO login is currently unsupported.

  - Is the Drupal site using ajax login?
      Ajax login is currently unsupported.

  - Are all of the API keys properly set?

* If the paywall is not being show on the videos:

  - Is the video provider Ooyala?
    Currently, only ooyala integration is supported.

  - Is the Ooyala module enabled and being used to display videos?

  - Is the invideous_ooyala_integration submodule enabled?

* If the buy now button is not being displayed:

  - Has the configuration setting been enabled?

  - Has the invideous_ooyala_integration submodule been enabled?

-- CONTACT --

Current maintainers:
* Ken Gilbert (kengib) - https://www.drupal.org/user/2754331

This project has been sponsored by:
* Achieve Internet
  http://www.achieveinternet.com
