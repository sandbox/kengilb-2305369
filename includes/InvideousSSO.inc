<?php
/**
 * @file
 * Single sign on implementation for the invideous service.
 */

/**
 * SSO Client Class that provides Single Sign-On functionality for Invideous Affiliates.
 *
 * @package    Invideous.SSO
 * @author     Rubin Gjerovski, Sinisa Mamuzik
 */
class InvideousSsoClient {
  private $_server_url = 'http://api.invideous.com/sso/server.php';
  private $_client_name;
  private $_secret_code;

  // Do not assign values below
  public $response;
  public $user_data;
  public $error_message;

  private $_cookie_name;
  private $_sso_check_cookie_name;
  private $_client_session_id;
  private $_remote_address;

  public function __construct() {

    $this->_client_name = variable_get('invideous_sso_name', '');
    $this->_secret_code = variable_get('invideous_sso_key', '');
    $this->_cookie_name = strtolower($this->_client_name . '_client_session_id');
    $this->_sso_check_cookie_name = strtolower($this->_client_name . '_sso_check');

    $_SERVER["REQUEST_URI"] = preg_replace('/(\?|&)inv_sso_check=true/', '', $_SERVER["REQUEST_URI"]);

    $this->_get_remote_address();
    $this->_set_client_session_cookie();
  }

  public function get_user_info() {
    $result = $this->_server_action('get_user_info');
    return $result;
  }

  public function login($username, $password, $remember = true, $platform = '') {
    $result = $this->_server_action('login', array('username' => $username, 'password' => $password, 'remember' => (int) $remember, 'platform' => $platform));
    return $result['status'] == 'success' ? true : false;
  }

  public function logout() {
    $result = $this->_server_action('logout');
    return $result['status'] == 'success' ? true : false;
  }

  public function is_ajax_request() {
    return (isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest');
  }

  // Private methods below
  private function _attach() {
    header("Location: " . $this->_get_attach_url() . "&redirect=" . urlencode("http://{$_SERVER["SERVER_NAME"]}{$_SERVER["REQUEST_URI"]}"), true, 307);
    exit;
  }

  private function _redirect_to_base() {
    header("Location: http://{$_SERVER["SERVER_NAME"]}{$_SERVER["REQUEST_URI"]}", true, 307);
    exit;
  }

  private function _get_attach_url() {
    return $this->_server_url . '?action=attach&client_name=' . $this->_client_name . '&client_session_id=' . $this->_client_session_id . '&checksum=' . $this->_generate_checksum();
  }

  private function _get_client_session_token() {
    return 'SSO-' . $this->_client_name . '-' . $this->_client_session_id . '-' . md5('session' . $this->_client_session_id . $this->_secret_code);
  }

  private function _get_remote_address() {

    $remote_address = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];

    $this->_remote_address = $remote_address;

    return $remote_address;
  }

  private function _set_client_session_cookie() {

    if (!isset($_COOKIE[$this->_cookie_name])) {
      $this->_client_session_id = $this->_generate_client_session_id();
      setcookie($this->_cookie_name, $this->_client_session_id, 0, '/');
    }
    else
      $this->_client_session_id = $_COOKIE[$this->_cookie_name];
  }

  private function _validate_check_cookie() {

    if (!isset($_COOKIE[$this->_sso_check_cookie_name])) {

      if (isset($_GET['inv_sso_check'])) {
        throw new Exception("SSO failure: Cookies are not enabled", 1300);
      } else {
        $this->_set_check_cookie();
      }
    } else {

      if ($_COOKIE[$this->_sso_check_cookie_name] > 3) {
        $this->_set_check_cookie('infinite_loop');
        $this->_redirect_to_base();
      }
      else if($_COOKIE[$this->_sso_check_cookie_name] == 'infinite_loop') {
        $this->_set_check_cookie();
        throw new Exception("SSO failure: An infinite redirect has been detected", 1400);
      } else {
        $this->_set_check_cookie(++$_COOKIE[$this->_sso_check_cookie_name]);
      }
    }
  }

  private function _set_check_cookie($value = 1) {
    setcookie($this->_sso_check_cookie_name, $value, time() + 2592000, '/');
  }

  private function _generate_client_session_id() {
    return md5(uniqid(mt_rand(), true));
  }

  private function _generate_checksum() {
    return md5('attach' . $this->_client_session_id . $this->_secret_code);
  }

  private function _server_action($action, $data = null) {

    $url_to_call = $this->_server_url . "?action=" . $action;

    if ($action != 'get_ip_address')
      $url_to_call .= "&client_session_token=" . $this->_get_client_session_token();

    $ch = curl_init($url_to_call);

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    if (isset($data)) {
      curl_setopt($ch, CURLOPT_POST, true);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    }

    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $response = curl_exec($ch);

    if (curl_errno($ch) != 0)
      throw new Exception("SSO failure: HTTP request to server failed", 1000);

    $result = json_decode($response, true);

    if (!is_array($result)) {
      throw new Exception("SSO failure: The server returned Bad JSON data", 1100);
    }

    if ($result['response']['status'] == "error" && $result['response']['error_code'] == 500)
      throw new Exception($result['response']['message'], 1200);

    if ($result['response']['status'] == "error" && ($result['response']['error_code'] == 600 || $result['response']['error_code'] == 700)) {

      $this->_validate_check_cookie();

      if (!$this->is_ajax_request()) {
        $this->_attach();
      } else {
        $result['response']['message'] = 'attach';
        $result['response']['result'] = $this->_get_attach_url();
      }
    }

    if ($result['response']['status'] == "error")
      $this->error_message = $result['response']['message'];

    if (isset($result['response']['result']['user_info']))
      $this->user_data = $result['response']['result']['user_info'];

    if ($result['response']['status'] == "success" && isset($_GET['inv_sso_check'])) {
      $this->_redirect_to_base();
    }

    $this->response = $result['response'];

    return $result['response'];
  }
}
