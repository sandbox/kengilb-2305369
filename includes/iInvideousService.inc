<?php
/**
 * @file
 * Defines the interface for the Invideous web service.
 */

/**
 * Interface for the Invideous web service.
 */
interface iInvideousService {

  //region AccountManagement.
  /**
   * Plugin/category api call that will send a reference to the video
   * to the Invideous system.
   *
   * @param string $source_url
   *   The source url of where the video exists.
   *
   * @param string $ovp_name
   *   The name of the video provider being utilized.
   *   i.e. ooyala
   *
   * @param string $ovp_video_id
   *   The video id provided by the video provided specified
   *   above.
   *
   * @param string $referrer
   *   The referring site from where the API is called.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @param string $category
   *   The category/subcategory of the api call.
   *
   * @return bool
   *   Returns a boolean to determine whether or not the operation
   *   was successful.
   */
  public function referenceVideo($source_url, $ovp_name, $ovp_video_id, $referrer, $response_type, $category);

  /**
   * Write api call that will set a referenced video to an Invideous app.
   *
   * @param string $ovp_name
   *   The name of the video provider being utilized.
   *   i.e. ooyala
   *
   * @param string $ovp_video_id
   *   The video id provided by the video provided specified
   *   above.
   *
   * @param int $app_id
   *   The Invideous id associated with the desired app.
   *   Use getVideoApps to find the numeric id.
   *
   * @param array $app_data
   *   An array of optional parameters to send to Invideous
   *   about how app specific settings should be set.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns a boolean indicating whether or not the video
   *   was successfully set to the application.
   */
  public function setVideoApp($ovp_name, $ovp_video_id, $app_id, $app_data, $response_type);

  /**
   * Read api call that will gather administrative information about
   * all of the Invideous apps associated with the account.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of information about the video apps associated
   *   with the Invideous account. Otherwise, returns an empty array.
   */
  public function getVideoApps($response_type);

  /**
   * Read api call that will gather information about what
   * videos are associated with the Invideous system.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of available videos.
   *   Otherwise, returns an empty array.
   */
  public function getVideos($response_type);

  /**
   * Category api call that will gather the details of a video from the
   * video provider (OVP).
   *
   * @param string $video_id
   *   The Invideous video id.
   *
   * @param string $referrer
   *   The referring page.
   *
   * @param string $ovp_name
   *   The name of the video provider.
   *
   * @param string $ovp_video_id
   *   The unique video id provided by the video provider.
   *
   * @param string $plugin_version
   *   Plugin version.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of details from Invideous, otherwise returns
   *   an empty array.
   */
  public function getVideoDetails($video_id, $referrer, $ovp_name, $ovp_video_id, $plugin_version, $category, $response_type);

  /**
   * Category api call that will gather the details of multiple videos from Invideous
   * and the video provider.
   *
   * @param array $invideos_video_ids
   *   An array of invideous video ids in the format:
   *   $invideous_video_ids = array('videoid1','videoid2','videoid3');
   *
   * @param array $ovp_video_ids
   *   An array of ovp video ids in the format:
   *   $ovp_video_ids = array('videoid1','videoid2','videoid3');
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of data about the videos if successful,
   *   otherwise returns an empty array.
   */
  public function getMultipleVideoDetails($invideos_video_ids, $ovp_video_ids, $category, $response_type);

  /**
   * Category api call that will gather all of the packages available to
   * the Invideous account.
   *
   * @param int $records_per_page
   *   Number of records to show per page.
   *
   * @param int $page
   *   The page number to show.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of the avilable packages, otherwise
   *   returns an empty array.
   */
  public function getPackages($records_per_page, $page, $category, $response_type);

  /**
   * Cateogry api call that gathers the details of the videos in the
   * specified package. NOTE: Either $package_id or $external_id
   * must be sent but NOT both.
   *
   * @param string $package_id
   *   The Invideous id of the package to acquire.
   *
   * @param string $external_id
   *   The OVP id of the package to acquire.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of information about the package videos, otherwise
   *   an empty array.
   */
  public function getPackageVideos($package_id, $external_id, $category, $response_type);

  /**
   * Category api call that will gather administrative information
   * about the package itself.
   *
   * @param string $package_id
   *   The Invideous id of the package to acquire.
   *
   * @param string $external_id
   *   The external id of the package to acquire.
   *
   * @param string $session_id
   *   The session id the logged in user. Necessary if
   *   Invideous cookies are not set.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of package data if successful, otherwise
   *   returns an empty array.
   */
  public function getPackageDetails($package_id, $external_id, $session_id, $category, $response_type);

  /**
   * Category api call that will gather information about what
   * videos are available.
   *
   * @param string $category
   *   The category/subcategory of the api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of available videos.
   *   Otherwise, returns an empty array.
   */
  public function getOvpVideos($category, $response_type);

  /**
   * Read api call that will retrieve information about
   * the sales of the account or a specific video.
   *
   * @param string $date_from
   *   Start date in the format YYYY-MM-DD
   *
   * @param string $date_to
   *   End date in the format YYYY-MM-DD
   *
   * @param string $referrer
   *   Filters results based on the referrer set.
   *   If none was originally set, use 'has_no_value'.
   *
   * @param string $ovp_name
   *   Video provider name. If used, must be used
   *   with the $ovp_video_id.
   *
   * @param string $ovp_video_id
   *   Video provider unique video id.
   *
   * @param string $sort_by
   *   Use one of the following sorting options:
   *     customer_email_asc	– Ascending	order	by customer	emails.
   *     customer_email_desc - Descending order by customer	emails.
   *     video_id_asc	–	Ascending	order	by video_id.
   *     video_id_desc – Descending	order	by video_id.
   *     country_asc	–	Ascending	order	by country name from which
   *       purchase	was	made.
   *     country_desc	–	Descending order by	country	name from	which
   *       purchase	was	made.
   *     referrer_asc	–	Ascending	order	by referrer	value.
   *     referrer_desc – Descending	order	by referrer value.
   *     created_asc – Ascending order by	transaction	creation date.
   *     created_desc	–	Descending order by	transaction creation date.
   *
   * @param int $page_number
   *   The number page that should be retrieved.
   *
   * @param int $records_per_page
   *   The number of records to show per page.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of info. about sales, otherwise
   *   returns an empty array.
   */
  public function getSales($date_from, $date_to, $referrer, $ovp_name, $ovp_video_id, $sort_by, $page_number, $records_per_page, $response_type);

  /**
   * Write api call that will register a consumer with an
   * invideous account.
   *
   * @param string $user_name
   *   The user name of publisher to add.
   *
   * @param string $password
   *   The login password.
   *
   * @param string $repeat_password
   *   Confirmation of the login password.
   *
   * @param string $email
   *   The email to register the new publisher to.
   *
   * @param string $full_name
   *   The full name of the publisher.
   *
   * @param string $referrer
   *   The site from which this publisher is being registered.
   *
   * @param string $client_ip
   *   Used for data logging purposes.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function registerConsumer($user_name, $password, $repeat_password, $email, $full_name, $referrer, $client_ip, $response_type);

  /**
   * Read api call that will check to see if a username or
   * email is available/already registered.
   *
   * @param string $user_name
   *   The user name in question.
   *
   * @param string $email
   *   The email in question.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if available, otherwise false.
   */
  public function checkAvailableConsumer($user_name, $email, $response_type);
  //endregion.

  //region ClientManagement.
  /**
   * Category API call that will register either a consumer or a producer
   * to the Invideous system.
   *
   * @param string $email
   *   The email to register.
   *
   * @param string $password
   *   Password to register.
   *
   * @param string $repeat_password
   *   Password confirmation.
   *
   * @param string $category
   *   The API category.
   *
   * @param string $user_name
   *   The username to register with the account.
   *
   * @param string $full_name
   *   The full name to register with the account.
   *
   * @param string $role
   *   Can be either consumer or publisher.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function registerGeneral($email, $password, $repeat_password, $category, $user_name, $full_name, $role, $response_type);

  /**
   * Cateogry API call that automatically generates a
   * username and password for a user based on the given email.
   *
   * @param string $email
   *   The email to create an account for.
   *
   * @param string $category
   *   The API category.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function autoRegister($email, $category, $response_type);

  /**
   * Category API call that logs an Invideous user into the system.
   *
   * @param string $user_name
   *   The username of the Invideous user.
   *
   * @param string $password
   *   The Invideous user password.
   *
   * @param string $platform
   *   Can be either web or mobile.
   *
   * @param string $category
   *   The API category.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of user info. if successful, otherwise an empty array.
   */
  public function login($user_name, $password, $platform, $category, $response_type);

  /**
   * Category API call that will logout a user based on the
   * session id.
   *
   * @param string $session_id
   *   The Invideous session id. Can
   *   be captured when first logging
   *   in a user.
   *
   * @param string $category
   *   The category of API call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function logout($session_id, $category, $response_type);

  /**
   * Category API call that gather information about
   * either the currently logged in user or a specified
   * session id.
   *
   * @param string $session_id
   *   The Invideous session id. Can
   *   be captured when first logging
   *   in a user.
   *
   * @param string $category
   *   The API call category.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of information about the user if
   *   successful, otherwise an empty array.
   */
  public function getUserInfo($session_id, $category, $response_type);

  /**
   * Category API call that will prompt the Invideous
   * system to reset the password of the Invideous account.
   * Only one parameter, $user_name or $email must be sent.
   *
   * @param string $email
   *   The email of the Invideous account to reset
   *   the password for.
   *
   * @param string $user_name
   *   The username of the Invideous account to
   *   reset the password for.
   *
   * @param string $referrer
   *   The url of the page or video from which
   *   the reset requested.
   *
   * @param string $category
   *   The category of API call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function resetPassword($email, $user_name, $referrer, $category, $response_type);

  /**
   * Read API call that will gather details about all
   * of the transactions that have taken place for the
   * Invideous account.
   *
   * @param int $count
   *   The number of records to display per page.
   *
   * @param int $page
   *   The page number to display.
   *
   * @param bool $only_active
   *   Boolean indicating whether or not to filter by
   *   transactions with active access.
   *
   * @param string $extra_fields
   *   Retrieves additional data based on the type of
   *   data. Currently, only 'payment_custom_data' is
   *   possible.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of transactions if successful,
   *   otherwise returns an empty array.
   */
  public function getUserTransactions($count, $page, $only_active, $extra_fields, $response_type);

  /**
   * Read API call that will return the details of
   * a specific transaction, given a transaction id.
   *
   * @param int $transaction_id
   *   The id of the transaction to pull up.
   *
   * @param int $page
   *   The page number of the transaction.
   *
   * @param bool $include_tax_relief_data
   *   Determines whether or not to include
   *   the tax relief data with the transaction
   *   details.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of transaction details if successful,
   *   otherwise returns an empty array.
   */
  public function getTransactionDetails($transaction_id, $page, $include_tax_relief_data, $response_type);

  /**
   * Read API call that will gather data about all of the
   * user paid subscriptions related to the Invideous account.
   *
   * @param int $count
   *   The number of records to retrieve.
   *
   * @param int $page
   *   The page of the record to retrieve.
   *
   * @param bool $only_active
   *   Filters by only active subscriptions
   *   if true.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of user paid subscriptions if
   *   successful, otherwise returns an empty array.
   */
  public function getUserPaidSubscriptions($count, $page, $only_active, $response_type);

  /**
   * Read API call that will gather data about all
   * of the user paid playlists.
   *
   * @param int $count
   *   The number of records to retrieve.
   *
   * @param int $page
   *   The page number to retrieve.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of user paid playlist data if
   *   successful, otherwise an empty array.
   */
  public function getUserPaidPlaylists($count, $page, $response_type);

  /**
   * Read API call that will gather data about
   * the subscription tariffs tied to the account.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of subscription tariff data if
   *   successful, otherwise returns an empty array.
   */
  public function getSubscriptionsTariffs($response_type);

  /**
   * Category API call that will gather data about the purchased
   * items for an individual user. Requires the session id if the
   * API call is not being made from a logged in user with a
   * valid Invideous cookie.
   *
   * @param string $session_id
   *   The Invideous session id of the user to inquire for.
   *
   * @param int $offset
   *   The offset of the purchased items to retrieve.
   *   i.e. If there is a list of 10 and an offset or 2,
   *     items 3-10 will be retrieved.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of purchased items for the user if
   *   successful, otherwise returns an empty array.
   */
  public function getPurchasedItems($session_id, $offset, $category, $response_type);
  //endregion.
}
