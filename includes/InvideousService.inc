<?php
/**
 * @file
 * Service class for handling communication with the Invideous web service.
 */

/**
 * Handles administrative communication with Invideous.
 */
class InvideousService implements iInvideousService {
  protected $serviceBaseUrl;
  protected $servicePublicKey;
  protected $serviceSecretKey;
  protected $serviceReadToken;
  protected $servicePublisherId;

  /**
   * Constructor.
   */
  public function __construct() {
    $settings = invideous_get_config();

    // TODO: Make this adjustable?
    $this->serviceBaseUrl = 'http://api.invideous.com';

    $this->serviceSecretKey = $settings['secret_key'];
    $this->servicePublicKey = $settings['public_key'];
    $this->serviceReadToken = $settings['read_token'];
    $this->servicePublisherId = $settings['publisher_id'];
  }

  //region AccountManagement.
  /**
   * Plugin/category api call that will send a reference to the video
   * to the Invideous system.
   *
   * @param string $source_url
   *   The source url of where the video exists.
   *
   * @param string $ovp_name
   *   The name of the video provider being utilized.
   *   i.e. ooyala
   *
   * @param string $ovp_video_id
   *   The video id provided by the video provided specified
   *   above.
   *
   * @param string $referrer
   *   The referring site from where the API is called.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @param string $category
   *   The category/subcategory of the api call.
   *
   * @return bool
   *   Returns a boolean to determine whether or not the operation
   *   was successful.
   */
  public function referenceVideo($source_url, $ovp_name, $ovp_video_id, $referrer, $response_type = 'json', $category = 'plugin') {
    $data = array(
      'source_url' => $source_url,
      'ovp_name' => $ovp_name,
      'ovp_video_id' => $ovp_video_id,
      'referrer' => $referrer,
      'publisher_id' => $this->servicePublisherId,
    );
    $service_path = '/' . $category . '/reference_video';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;

    return $success;
  }

  /**
   * Write api call that will set a referenced video to an Invideous app.
   *
   * @param string $ovp_name
   *   The name of the video provider being utilized.
   *   i.e. ooyala
   *
   * @param string $ovp_video_id
   *   The video id provided by the video provided specified
   *   above.
   *
   * @param int $app_id
   *   The Invideous id associated with the desired app.
   *   Use getVideoApps to find the numeric id.
   *
   * @param array $app_data
   *   An array of optional parameters to send to Invideous
   *   about how app specific settings should be set.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns a boolean indicating whether or not the video
   *   was successfully set to the application.
   */
  public function setVideoApp($ovp_name, $ovp_video_id, $app_id, $app_data = array(), $response_type = 'json') {
    $data = array(
      'ovp_name' => $ovp_name,
      'ovp_video_id' => $ovp_video_id,
      'app_id' => $app_id,
      'app_data' => $app_data,
    );
    $service_path = '/write/set_video_app';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'write');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;
    if (!$success && !empty($response['response']['message'])) {
      // Take special notice of apps that are not set,
      // as this might cause paid vidoes to go free unnoticed.
      watchdog(INVIDEOUS_WATCHDOG, 'A video was not able to have an app set by Invideous.' . '<pre>Error Message Returned: ' . print_r($response['response']['message'], TRUE) . '</pre>', array(), WATCHDOG_ERROR);
    }

    return $success;
  }

  /**
   * Read api call that will gather administrative information about
   * all of the Invideous apps associated with the account.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of information about the video apps associated
   *   with the Invideous account. Otherwise, returns an empty array.
   */
  public function getVideoApps($response_type = 'json') {
    $service_path = '/read/get_video_apps';
    $response = $this->SubmitRequest($service_path, 'POST', array(), $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, array(), $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Read api call that will gather information about what
   * videos are associated with the Invideous system.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of available videos.
   *   Otherwise, returns an empty array.
   */
  public function getVideos($response_type = 'json') {
    $service_path = '/read/get_videos';
    $response = $this->SubmitRequest($service_path, 'POST', array(), $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, array(), $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category api call that will gather the details of a video from the
   * video provider (OVP).
   *
   * @param string $video_id
   *   The Invideous video id.
   *
   * @param string $referrer
   *   The referring page.
   *
   * @param string $ovp_name
   *   The name of the video provider.
   *
   * @param string $ovp_video_id
   *   The unique video id provided by the video provider.
   *
   * @param string $plugin_version
   *   Plugin version.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of details from Invideous, otherwise returns
   *   an empty array.
   */
  public function getVideoDetails($video_id, $referrer, $ovp_name, $ovp_video_id, $plugin_version = '', $category = 'plugin', $response_type = 'json') {
    $data = array(
      'video_id' => $video_id,
      'referrer' => $referrer,
      'ovp_name' => $ovp_name,
      'ovp_video_id' => $ovp_video_id,
      'plugin_version' => $plugin_version,
    );
    $service_path = '/' . $category . '/get_video_details';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category api call that will gather the details of multiple videos from Invideous
   * and the video provider.
   *
   * @param array $invideos_video_ids
   *   An array of invideous video ids in the format:
   *   $invideous_video_ids = array('videoid1','videoid2','videoid3');
   *
   * @param array $ovp_video_ids
   *   An array of ovp video ids in the format:
   *   $ovp_video_ids = array('videoid1','videoid2','videoid3');
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of data about the videos if successful,
   *   otherwise returns an empty array.
   */
  public function getMultipleVideoDetails($invideos_video_ids, $ovp_video_ids = array(), $category = 'plugin', $response_type = 'json') {
    $videos = !empty($invideos_video_ids) ? implode(',', $invideos_video_ids) : '';
    $ovp_videos = !empty($ovp_video_ids) ? implode(',', $ovp_video_ids) : '';

    $data = array(
      'videos' => $videos,
      'ovp_videos' => $ovp_videos,
      'publisher_id' => $this->servicePublisherId,
    );
    $service_path = '/' . $category . '/get_videos_details';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category api call that will gather all of the packages available to
   * the Invideous account.
   *
   * @param int $records_per_page
   *   Number of records to show per page.
   *
   * @param int $page
   *   The page number to show.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of the avilable packages, otherwise
   *   returns an empty array.
   */
  public function getPackages($records_per_page = 1000, $page = 1, $category = 'plugin', $response_type = 'json') {
    $data = array(
      'records_per_page' => $records_per_page,
      'page' => $page,
      'publisher_id' => $this->servicePublisherId,
    );
    $service_path = '/' . $category . '/get_packages';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Cateogry api call that gathers the details of the videos in the
   * specified package. NOTE: Either $package_id or $external_id
   * must be sent but NOT both.
   *
   * @param string $package_id
   *   The Invideous id of the package to acquire.
   *
   * @param string $external_id
   *   The OVP id of the package to acquire.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of information about the package videos, otherwise
   *   an empty array.
   */
  public function getPackageVideos($package_id, $external_id = '', $category = 'plugin', $response_type = 'json') {
    if (!empty($package_id)) {
      $data = array(
        'package_id' => $package_id,
      );
    }
    else {
      $data = array(
        'external_id' => $external_id,
        'publisher_id' => $this->servicePublisherId,
      );
    }

    $service_path = '/' . $category . '/get_package_videos';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category api call that will gather administrative information
   * about the package itself.
   *
   * @param string $package_id
   *   The Invideous id of the package to acquire.
   *
   * @param string $external_id
   *   The external id of the package to acquire.
   *
   * @param string $session_id
   *   The session id the logged in user. Necessary if
   *   Invideous cookies are not set.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of package data if successful, otherwise
   *   returns an empty array.
   */
  public function getPackageDetails($package_id, $external_id, $session_id = '', $category = 'plugin', $response_type = 'json') {
    if (!empty($package_id)) {
      $data = array(
        'package_id' => $package_id,
      );
    }
    else {
      $data = array(
        'external_id' => $external_id,
        'publisher_id' => $this->servicePublisherId,
      );
    }

    if (!empty($session_id)) {
      $data['session_id'] = $session_id;
    }

    $service_path = '/' . $category . '/get_package_details';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category api call that will gather information about what
   * videos are available.
   *
   * @param string $category
   *   The category/subcategory of the api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of available videos.
   *   Otherwise, returns an empty array.
   */
  public function getOvpVideos($category = 'plugin', $response_type = 'json') {
    $service_path = '/' . $category . '/get_videos';
    $response = $this->SubmitRequest($service_path, 'POST', array(), $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, array(), $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Read api call that will retrieve information about
   * the sales of the account or a specific video.
   *
   * @param string $date_from
   *   Start date in the format YYYY-MM-DD
   *
   * @param string $date_to
   *   End date in the format YYYY-MM-DD
   *
   * @param string $referrer
   *   Filters results based on the referrer set.
   *   If none was originally set, use 'has_no_value'.
   *
   * @param string $ovp_name
   *   Video provider name. If used, must be used
   *   with the $ovp_video_id.
   *
   * @param string $ovp_video_id
   *   Video provider unique video id.
   *
   * @param string $sort_by
   *   Use one of the following sorting options:
   *     customer_email_asc	– Ascending	order	by customer	emails.
   *     customer_email_desc - Descending order by customer	emails.
   *     video_id_asc	–	Ascending	order	by video_id.
   *     video_id_desc – Descending	order	by video_id.
   *     country_asc	–	Ascending	order	by country name from which
   *       purchase	was	made.
   *     country_desc	–	Descending order by	country	name from	which
   *       purchase	was	made.
   *     referrer_asc	–	Ascending	order	by referrer	value.
   *     referrer_desc – Descending	order	by referrer value.
   *     created_asc – Ascending order by	transaction	creation date.
   *     created_desc	–	Descending order by	transaction creation date.
   *
   * @param int $page_number
   *   The number page that should be retrieved.
   *
   * @param int $records_per_page
   *   The number of records to show per page.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of info. about sales, otherwise
   *   returns an empty array.
   */
  public function getSales($date_from = '', $date_to = '', $referrer = '', $ovp_name = '', $ovp_video_id = '', $sort_by = '', $page_number = 1, $records_per_page = 20, $response_type = 'json') {
    $data = array(
      'pagenumber' => $page_number,
      'records_per_page' => $records_per_page,
    );
    if (!empty($date_from) && !empty($date_to)) {
      $data['date_from'] = $date_from;
      $data['date_to'] = $date_to;
    }
    if (!empty($referrer)) {
      $data['referrer'] = $referrer;
    }
    if (!empty($ovp_name) && !empty($ovp_video_id)) {
      $data['ovp_name'] = $ovp_name;
      $data['ovp_video_id'] = $ovp_video_id;
    }
    if (!empty($sort_by)) {
      $data['sort_by'] = $sort_by;
    }

    $service_path = '/read/publisher_sales';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Write api call that will register a new consumer with an
   * invideous account.
   *
   * @param string $user_name
   *   The user name of publisher to add.
   *
   * @param string $password
   *   The login password.
   *
   * @param string $repeat_password
   *   Confirmation of the login password.
   *
   * @param string $email
   *   The email to register the new publisher to.
   *
   * @param string $full_name
   *   The full name of the publisher.
   *
   * @param string $referrer
   *   The site from which this publisher is being registered.
   *
   * @param string $client_ip
   *   Used for data logging purposes.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, else false.
   */
  public function registerConsumer($user_name, $password, $repeat_password, $email, $full_name = '', $referrer = '', $client_ip = '', $response_type = 'json') {
    $data = array(
      'username' => $user_name,
      'password' => $password,
      'repeat_password' => $repeat_password,
      'email' => $email,
      'fullname' => $full_name,
      'referrer' => $referrer,
      'client_ip' => $client_ip,
    );
    $service_path = '/write/register_user';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'write');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;

    return $success;
  }

  /**
   * Read api call that will check to see if a username or
   * email is available/already registered.
   *
   * @param string $user_name
   *   The user name in question.
   *
   * @param string $email
   *   The email in question.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if available, otherwise false.
   */
  public function checkAvailableConsumer($user_name, $email, $response_type = 'json') {
    $data = array();
    if (!empty($user_name)) {
      $data['username'] = $user_name;
    }
    if (!empty($email)) {
      $data['email'] = $email;
    }

    $service_path = '/read/check_available_user';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $available = (!empty($response['response']['result'])) ? $response['response']['result'] : FALSE;
    return $available;
  }
  //endregion.

  //region ClientManagement.
  /**
   * Category API that will register either a consumer or a producer
   * to the Invideous system.
   *
   * @param string $email
   *   The email to register.
   *
   * @param string $password
   *   Password to register.
   *
   * @param string $repeat_password
   *   Password confirmation.
   *
   * @param string $category
   *   The API category.
   *
   * @param string $user_name
   *   The username to register with the account.
   *
   * @param string $full_name
   *   The full name to register with the account.
   *
   * @param string $role
   *   Can be either consumer or publisher.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function registerGeneral($email, $password, $repeat_password, $category = 'plugin', $user_name = '', $full_name = '', $role = 'consumer', $response_type = 'json') {
    $data = array(
      'email' => $email,
      'password' => $password,
      'repeat_password' => $repeat_password,
      'username' => $user_name,
      'fullname' => $full_name,
      'role' => $role,
      'publisher_id' => $this->servicePublisherId,
    );
    $service_path = '/' . $category . '/register';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;

    return $success;
  }

  /**
   * Cateogry API call that automatically generates a
   * username and password for a user based on the given email.
   *
   * @param string $email
   *   The email to create an account for.
   *
   * @param string $category
   *   The API category.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function autoRegister($email, $category = 'plugin', $response_type = 'json') {
    $data = array(
      'email' => $email,
      'publisher_id' => $this->servicePublisherId,
    );
    $service_path = '/' . $category . '/set_client';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;

    return $success;
  }

  /**
   * Category API call that logs an Invideous user into the system.
   *
   * @param string $user_name
   *   The username of the Invideous user.
   *
   * @param string $password
   *   The Invideous user password.
   *
   * @param string $platform
   *   Can be either web or mobile.
   *
   * @param string $category
   *   The API category.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of user info. if successful, otherwise an empty array.
   */
  public function login($user_name, $password, $platform = 'web', $category = 'plugin', $response_type = 'json') {
    $data = array(
      'username' => $user_name,
      'password' => $password,
      'platform' => $platform,
    );
    $service_path = '/' . $category . '/login';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category API call that will logout a user based on the
   * session id.
   *
   * @param string $session_id
   *   The Invideous session id. Can
   *   be captured when first logging
   *   in a user.
   *
   * @param string $category
   *   The category of API call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function logout($session_id, $category = 'plugin', $response_type = 'json') {
    $data = array(
      'session_id' => $session_id,
    );
    $service_path = '/' . $category . '/logout';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;

    return $success;
  }

  /**
   * Category API call that gather information about
   * either the currently logged in user or a specified
   * session id.
   *
   * @param string $session_id
   *   The Invideous session id. Can
   *   be captured when first logging
   *   in a user.
   *
   * @param string $category
   *   The API call category.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of information about the user if
   *   successful, otherwise an empty array.
   */
  public function getUserInfo($session_id = '', $category = 'plugin', $response_type = 'json') {
    if (!empty($session_id)) {
      $data = array('session_id' => $session_id);
    }
    else {
      $data = array();
    }

    $service_path = '/' . $category . '/get_user_info';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category API call that will prompt the Invideous
   * system to reset the password of the Invideous account.
   * Only one parameter, $user_name or $email must be sent.
   *
   * @param string $email
   *   The email of the Invideous account to reset
   *   the password for.
   *
   * @param string $user_name
   *   The username of the Invideous account to
   *   reset the password for.
   *
   * @param string $referrer
   *   The url of the page or video from which
   *   the reset requested.
   *
   * @param string $category
   *   The category of API call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return bool
   *   Returns true if successful, otherwise false.
   */
  public function resetPassword($email = '', $user_name = '', $referrer = '', $category = 'plugin', $response_type = 'json') {
    $data = array('referrer' => $referrer);
    if (!empty($email)) {
      $data['email'] = $email;
    }
    else {
      $data['username'] = $user_name;
    }

    $service_path = '/' . $category . '/forgot_password';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    $success = (!empty($response['response']['status']) && $response['response']['status'] == 'success') ? TRUE : FALSE;

    return $success;
  }

  /**
   * Read API call that will gather details about all
   * of the transactions that have taken place for the
   * Invideous account.
   *
   * @param int $count
   *   The number of records to display per page.
   *
   * @param int $page
   *   The page number to display.
   *
   * @param bool $only_active
   *   Boolean indicating whether or not to filter by
   *   transactions with active access.
   *
   * @param string $extra_fields
   *   Retrieves additional data based on the type of
   *   data. Currently, only 'payment_custom_data' is
   *   possible.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of transactions if successful,
   *   otherwise returns an empty array.
   */
  public function getUserTransactions($count = 0, $page = 0, $only_active = FALSE, $extra_fields = '', $response_type = 'json') {
    $data = array();
    if ($count != 0 && $page != 0) {
      $data['count'] = $count;
      $data['page'] = $page;
    }
    if ($only_active) {
      $data['only_active'] = 'T';
    }
    if (!empty($extra_fields)) {
      $data['extra_fields'] = $extra_fields;
    }

    $service_path = '/read/user_transactions';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Read API call that will return the details of
   * a specific transaction, given a transaction id.
   *
   * @param int $transaction_id
   *   The id of the transaction to pull up.
   *
   * @param int $page
   *   The page number of the transaction.
   *
   * @param bool $include_tax_relief_data
   *   Determines whether or not to include
   *   the tax relief data with the transaction
   *   details.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of transaction details if successful,
   *   otherwise returns an empty array.
   */
  public function getTransactionDetails($transaction_id, $page = 0, $include_tax_relief_data = FALSE, $response_type = 'json') {
    $data = array(
      'transaction_id' => $transaction_id,
    );
    if ($page != 0) {
      $data['page'] = $page;
    }
    if ($include_tax_relief_data) {
      $data['include_tax_relief_data'] = $include_tax_relief_data;
    }

    $service_path = '/read/transaction_details';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Read API call that will gather data about all of the
   * user paid subscriptions related to the Invideous account.
   *
   * @param int $count
   *   The number of records to retrieve.
   *
   * @param int $page
   *   The page of the record to retrieve.
   *
   * @param bool $only_active
   *   Filters by only active subscriptions
   *   if true.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of user paid subscriptions if
   *   successful, otherwise returns an empty array.
   */
  public function getUserPaidSubscriptions($count = 0, $page = 0, $only_active = FALSE, $response_type = 'json') {
    $data = array();
    if ($count != 0 && $page != 0) {
      $data['count'] = $count;
      $data['page'] = $page;
    }
    if ($only_active) {
      $data['only_active'] = 'T';
    }

    $service_path = '/read/user_paid_subscriptions';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Read API call that will gather data about all
   * of the user paid playlists.
   *
   * @param int $count
   *   The number of records to retrieve.
   *
   * @param int $page
   *   The page number to retrieve.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of user paid playlist data if
   *   successful, otherwise an empty array.
   */
  public function getUserPaidPlaylists($count = 0, $page = 0, $response_type = 'json') {
    $data = array();
    if ($count != 0 && $page != 0) {
      $data['count'] = $count;
      $data['page'] = $page;
    }

    $service_path = '/read/user_paid_playlists';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Read API call that will gather data about
   * the subscription tariffs tied to the account.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of subscription tariff data if
   *   successful, otherwise returns an empty array.
   */
  public function getSubscriptionsTariffs($response_type = 'json') {
    $service_path = '/read/get_subscriptions_tariffs';
    $response = $this->SubmitRequest($service_path, 'POST', array(), $response_type, 'read');
    $response = $this->ParseServiceResponse($response, $service_path, array(), $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  /**
   * Category API call that will gather data about the purchased
   * items for an individual user. Requires the session id if the
   * API call is not being made from a logged in user with a
   * valid Invideous cookie.
   *
   * @param string $session_id
   *   The Invideous session id of the user to inquire for.
   *
   * @param int $offset
   *   The offset of the purchased items to retrieve.
   *   i.e. If there is a list of 10 and an offset or 2,
   *     items 3-10 will be retrieved.
   *
   * @param string $category
   *   The category of api call.
   *
   * @param string $response_type
   *   The type of response desired from Invideous.
   *
   * @return array
   *   Returns an array of purchased items for the user if
   *   successful, otherwise returns an empty array.
   */
  public function getPurchasedItems($session_id = '', $offset = 0, $category = 'plugin', $response_type = 'json') {
    $data = array(
      'offset' => $offset,
    );
    if (!empty($session_id)) {
      $data['session_id'] = $session_id;
    }
    $service_path = '/' . $category . '/get_purchased_items';
    $response = $this->SubmitRequest($service_path, 'POST', $data, $response_type, $category);
    $response = $this->ParseServiceResponse($response, $service_path, $data, $response_type);

    if (!empty($response['response']['status']) && $response['response']['status'] == 'success') {
      return $response['response']['result'];
    }
    else {
      return array();
    }
  }

  //endregion.

  /**
   * Submits a request to the Invideous web service.
   *
   * @param string $path
   *   The path to the web service method being called.
   *   Format: /[api_category | api_subcategory]/[api_call_name]
   *
   * @param string $method
   *   The HTTP request method. Either 'GET' or 'POST'.
   *
   * @param array $data
   *   An array of name/value pairs that make up the body of the request.
   *
   * @param string $response_type
   *   The type of response requested from Invideous.
   *   Possible values: json, xml or debug.
   *
   * @param string $api_category
   *   The Invideous category/subcategory of this request.
   *   The API requests are authenticated/structured differently based on
   *   this parameter.
   *
   * @return object
   *   Returns the HTTP response object for the request.
   *
   * @throws InvideousAccessException
   *   Occurs if the user account submitting the request does not have
   *   the correct permissions.
   */
  protected function SubmitRequest($path, $method = 'POST', $data = array(), $response_type = 'json', $api_category = 'write') {
    if (!user_access(INVIDEOUS_PERM_USE_INVIDEOUS_SERVICE)) {
      throw new InvideousAccessException();
    }
    $request = array();

    // Build the request.
    $url = $this->serviceBaseUrl . $path;

    // TODO: Continually add support for more api categories here.
    switch ($api_category) {
      case 'read':
        // Use token based auth for read operations.
        $request['token'] = $this->serviceReadToken;

        // Fall through to plugin, it's the same as read with no token.
      case 'plugin':
        // Input the data query.
        if (!empty($data)) {
          foreach ($data as $key => $value) {
            if (!empty($value)) {
              $request[$key] = $value;
            }
          }
        }
        break;

      case 'write':
        // Use key based auth for write operations.
        $request['data'] = $data;
        $request['data'] = json_encode($request['data']);
        $request['sig'] = base64_encode(hash_hmac('sha1', $request['data'], $this->serviceSecretKey, TRUE));
        $request['key'] = $this->servicePublicKey;
        break;

      // This is most likely one of the category API calls,
      // most should follow suite with the plugin calling format.
      default:
        // Input the data query.
        if (!empty($data)) {
          foreach ($data as $key => $value) {
            if (!empty($value)) {
              $request[$key] = $value;
            }
          }
        }
        break;
    }

    // Specify the format we want the response from Invideous in.
    $request['type'] = $response_type;


    $options = array(
      'headers' => array(
        'Content-type' => 'application/x-www-form-urlencoded',
      ),
      'method' => $method,
      'max_redirects' => 1,
      'timeout' => 30,
      'data' => http_build_query($request),
    );
    $response = drupal_http_request($url, $options);

    return $response;
  }

  /**
   * Parses the HTTP response from the web service and returns the results.
   * Currently only supports json returned data. The parsing function will
   * spit back the raw xml and bebug strings for now.
   * TODO: Support formats other than json.
   *
   * @param object $response
   *   The HTTP response object.
   *
   * @param string $request
   *   The web service method requested.
   *
   * @param array $data
   *   An array of additional data passed with the request.
   *
   * @param string $request_type
   *   The string indicating what format the Invideous
   *   service api should return.
   *
   * @return array | string
   *   Returns an associative array containing the data response from the
   *   service call, otherwise an empty array. Also returns the xml or
   *   debug string if that's the return type that was requested.
   */
  protected function ParseServiceResponse($response, $request, $data = array(), $request_type = 'json') {
    if ($response && $response->code == '200') {
      switch ($request_type) {
        case 'json':
          $extracted_data = drupal_json_decode($response->data);
          break;

        case 'xml':
        case 'debug':
          $extracted_data = $response->data;
          break;
      }
    }
    else {
      $message = "
        Invideous Service request failed<br />\n
        Request: @request<br />\n
        Data:<br />\n
        <pre>@data</pre><br />\n
        Response:<br />\n
        <pre>@response</pre>
      ";
      $args = array(
        '@request' => $request,
        '@data' => print_r($data, TRUE),
        '@response' => print_r($response, TRUE),
      );
      watchdog(INVIDEOUS_WATCHDOG, $message, $args, WATCHDOG_ERROR);
      $extracted_data = array();
    }

    return $extracted_data;
  }
}
