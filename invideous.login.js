(function ($) {

  /**
   * Logs users into Invideous from Drupal systems that utilize ajax login.
   */
  Drupal.behaviors.invideousAjaxLogin = {
    attach: function(context, settings) {

      function cookie_iframe_load(src) {
        // We leave it up the login hook to finish the job after this
        // because relying purely on the JS is very inconsistent.
        $("#cookie_iframe").attr("src",src);
      }

      $("#edit-submit").live("click",function(event) {

        var password = $("#edit-pass").val();
        var username = $("#edit-name").val();
        var platform = "";

        // This is our first and only attempt at login to
        // Invideous via ajax. A login hook will handle
        // the second attempt.
        $.ajax({
          type: "POST",
          url: Drupal.settings.basePath + "invideous/ajax_login",
          data: "username="+username+"&password="+password+"&platform="+platform,
          dataType: "json",
          success: function(data){
            if (data.status != "error"){
              // Successful login, no action needed.
            }
            else if (data.message == "attach") {
              cookie_iframe_load(data.result);
            }
          }
        });
      });
    }
  }
})(jQuery);
