<?php
/**
 * @file
 * Administrative settings for the Invideous module.
 */

/**
 * Generates the Invideous settings form.
 *
 * @return array
 *   A form array for the settings form.
 */
function invideous_settings_form() {
  $form = array();

  $form['intro'] = array(
    '#prefix' => '<div class="invideous-intro">',
    '#suffix' => '</div>',
    '#markup' => t('This screen allows you to configure the Invideous service connection settings.'),
  );

  $form['invideous_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Access Key'),
    '#description' => t('Enter the secret access key provided by Invideous.'),
    '#default_value' => variable_get('invideous_secret_key', ''),
    '#required' => TRUE,
  );
  $form['invideous_public_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Public Key'),
    '#description' => t('Enter the public key provided by Invideous.'),
    '#default_value' => variable_get('invideous_public_key', ''),
    '#required' => TRUE,
  );
  $form['invideous_publisher_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Publisher ID'),
    '#description' => t('Enter the publisher ID provided by Invideous.'),
    '#default_value' => variable_get('invideous_publisher_id', ''),
    '#required' => TRUE,
  );
  $form['invideous_read_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Read Token'),
    '#description' => t('Enter the read token provided by Invideous.'),
    '#default_value' => variable_get('invideous_read_token', ''),
    '#required' => TRUE,
  );
  $form['invideous_prebooking'] = array(
    '#type' => 'select',
    '#title' => t('Show Buy Now Button'),
    '#description' => t('Show the buy now button anchored to the top of the video.'),
    '#options' => array(
      0 => t("Do Not Show"),
      1 => t("Show Event"),
      2 => t("Show Video"),
    ),
    '#default_value' => variable_get('invideous_prebooking', 0),
  );
  $form['invideous_automatic_registration'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Automatic Registration'),
    '#description' => t('Choose whether or not to have default drupal usernames and passwords set to the client Invideous account.'),
    '#default_value' => variable_get('invideous_automatic_registration', 0),
    '#required' => FALSE,
  );
  $form['invideous_sso'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable SSO'),
    '#description' => t('Choose whether or not to enable single sign on for users.'),
    '#default_value' => variable_get('invideous_sso', 0),
    '#required' => FALSE,
    '#states' => array(
      'invisible' => array(
        ':input[name="invideous_automatic_registration"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['invideous_sso_name'] = array(
    '#type' => 'textfield',
    '#title' => t('SSO Client Name'),
    '#description' => t('Enter the SSO client name provided by Invideous.'),
    '#default_value' => variable_get('invideous_sso_name', ''),
    '#states' => array(
      'invisible' => array(
        ':input[name="invideous_sso"]' => array('checked' => FALSE),
      ),
    ),
  );
  $form['invideous_sso_key'] = array(
    '#type' => 'textfield',
    '#title' => t('SSO Key'),
    '#description' => t('Enter the SSO secret key provided by Invideous.'),
    '#default_value' => variable_get('invideous_sso_key', ''),
    '#states' => array(
      'invisible' => array(
        ':input[name="invideous_sso"]' => array('checked' => FALSE),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Generates the Invideous tariff creation/update
 * form.
 *
 * @return array
 *   Returns the form array.
 */
function invideous_tariff_form() {
  $form = array();

  $form['wrapper_left'] = array(
    '#markup' => '<div class="invideous-wrapper-left" style="float: left; width: 35%;">',
  );
  $form['intro'] = array(
    '#prefix' => '<div class="invideous-intro">',
    '#suffix' => '</div>',
    '#markup' => t('This screen allows you to create and change Invideous tariffs.'),
  );
  $form['tariff_id'] = array(
    '#type' => 'select',
    '#title' => t('Tariff'),
    '#description' => t('Select the tariff to be added or edited.'),
    '#options' => invideous_get_tariff_select_list(),
    '#required' => TRUE,
  );
  $form['tariff_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Tariff Name'),
    '#description' => t('Enter a custom name for the tariff to easily identify later.'),
    '#required' => FALSE,
  );
  $form['tariff_price'] = array(
    '#type' => 'textfield',
    '#title' => t('Tariff Price'),
    '#description' => t('Enter the decimal price of the tariff.'),
    '#required' => TRUE,
  );
  $form['paywall_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Paywall Time'),
    '#description' => t('Enter the amount of time in seconds before the paywall appears.'),
    '#required' => TRUE,
  );
  $form['device_limit'] = array(
    '#type' => 'textfield',
    '#title' => t('Device Limit'),
    '#description' => t('Choose how many devices can view the video. Leave as 0 for unlimited'),
    '#default_value' => 0,
  );
  $form['tariff_type'] = array(
    '#type' => 'select',
    '#title' => t('Tariff Type'),
    '#description' => t('Select the type of tariff.'),
    '#options' => array(
      'general_ppv' => t('General PPV'),
      'general_ppv_views' => t('General PPV Number of Views'),
      'custom_ppv' => t('Custom PPV'),
      'general_sub' => t('General Subscription'),
      'custom_sub' => t('Custom Subscription'),
    ),
  );
  $form['tariff_views'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of Views'),
    '#description' => t('Select the number of views before the paywall appears again.'),
    '#states' => array(
      'visible' => array(
        ':input[name="tariff_type"]' => array('value' => 'general_ppv_views'),
      ),
    ),
  );
  $form['tariff_ppv_period_type'] = array(
    '#type' => 'select',
    '#title' => t('Tariff Period Type'),
    '#description' => t('Select the period denomination.'),
    '#options' => array(
      'minutes' => t('Minutes'),
      'hours' => t('Hours'),
      'days' => t('Days'),
      'week' => t('Weeks'),
      'month' => t('Months'),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="tariff_type"]' => array('value' => 'custom_ppv'),
      ),
    ),
  );
  $form['tariff_sub_period_type'] = array(
    '#type' => 'select',
    '#title' => t('Tariff Period Type'),
    '#description' => t('Select the period denomination.'),
    '#options' => array(
      'days' => t('Days'),
      'month' => t('Months'),
    ),
    '#states' => array(
      'visible' => array(
        ':input[name="tariff_type"]' => array('value' => 'custom_sub'),
      ),
    ),
  );
  $form['tariff_period_amount'] = array(
    '#type' => 'textfield',
    '#title' => t('Tariff Period Amount'),
    '#description' => t('Determine how long the tariff will last.'),
    '#states' => array(
      'visible' => array(
        ':input[name="tariff_type"]' => array(array('value' => 'custom_ppv'), array('value' => 'custom_sub')),
      ),
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['wrapper_left_end'] = array(
    '#markup' => '</div>',
  );
  $form['wrapper_right'] = array(
    '#markup' => '<div class="invideous-wrapper-right" style="float: right; width: 65%;">',
  );
  $form['table'] = array(
    '#markup' => invideous_generate_tariff_table(),
  );
  $form['wrapper_right_end'] = array(
    '#markup' => '</div>',
  );

  return $form;
}

/**
 * Validation handler for the tariff creation/update form.
 */
function invideous_tariff_form_validate($form, &$form_state) {
  $settings = $form_state['values'];
  if (!empty($settings['tariff_price']) && !is_numeric($settings['tariff_price'])) {
    form_set_error('invalid_data_type', t('Please enter a decimal number for the tariff price.'));
  }
  if (!empty($settings['paywall_time']) && !is_numeric($settings['paywall_time'])) {
    form_set_error('invalid_data_type', t('Please enter an integer for the paywall time.'));
  }
  if (!empty($settings['device_limit']) && !is_numeric($settings['device_limit'])) {
    form_set_error('invalid_data_type', t('Please enter an integer for device limit.'));
  }
  if (!empty($settings['tariff_views']) && !is_numeric($settings['tariff_views'])) {
    form_set_error('invalid_data_type', t('Please enter an integer for the number of video views.'));
  }
  if (!empty($settings['tariff_period_amount']) && !is_numeric($settings['tariff_period_amount'])) {
    form_set_error('invalid_data_type', t('Please enter an integer for the period amount.'));
  }
}

/**
 * Submit handler for the tariff creation/update form.
 */
function invideous_tariff_form_submit($form, &$form_state) {
  $settings = $form_state['values'];
  $tariff_id = $settings['tariff_id'];
  $tariff_name = (!empty($settings['tariff_name'])) ? $settings['tariff_name'] : $form['tariff_id']['#options'][$tariff_id];
  switch ($settings['tariff_type']) {
    case 'general_ppv':
      invideous_create_tariff($tariff_id, $tariff_name, $settings['tariff_price'], $settings['paywall_time'], '', 0, $settings['device_limit']);
      break;

    case 'general_ppv_views':
      invideous_create_tariff($tariff_id, $tariff_name, $settings['tariff_price'], $settings['paywall_time'], '', $settings['tariff_views'], $settings['device_limit']);
      break;

    case 'custom_ppv':
      $period = $settings['tariff_period_amount'] . ' ' . $settings['tariff_ppv_period_type'];
      invideous_create_tariff($tariff_id, $tariff_name, $settings['tariff_price'], $settings['paywall_time'], $period, 0, $settings['device_limit']);
      break;

    case 'general_sub':
      invideous_create_tariff($tariff_id, $tariff_name, $settings['tariff_price'], $settings['paywall_time'], '', 0, $settings['device_limit']);
      break;

    case 'custom_sub':
      $period = $settings['tariff_period_amount'] . ' ' . $settings['tariff_sub_period_type'];
      invideous_create_tariff($tariff_id, $tariff_name, $settings['tariff_price'], $settings['paywall_time'], $period, 0, $settings['device_limit']);
      break;
  }

  drupal_set_message(t('The tariff has been successfully saved.'));
}

/**
 * Helper function that creates a table out of the
 * available local tariff data.
 *
 * @return string
 *   Returns an html formatted string that
 *   represents all of the available local
 *   tariff data.
 */
function invideous_generate_tariff_table() {
  $header = array(
    'drupal_id' => array('data' => t('Drupal Id')),
    'tariff_id' => array('data' => t('Invideous Id')),
    'tariff_name' => array('data' => t('Name')),
    'tariff_price' => array('data' => t('Price')),
    'paywall_time' => array('data' => t('Time in Seconds')),
    'tariff_period' => array('data' => t('Period')),
    'tariff_times' => array('data' => t('Number of Views')),
    'device_access_limit' => array('data' => t('Device Limit')),
  );
  $rows = array();
  $existing_tariffs = invideous_get_all_tariffs();
  foreach ($existing_tariffs as $tariff) {
    $row = array(
      $tariff->drupal_id,
      $tariff->tariff_id,
      $tariff->tariff_name,
      $tariff->tariff_price,
      $tariff->paywall_time,
      $tariff->tariff_period,
      $tariff->tariff_times,
      $tariff->device_access_limit,
    );
    $rows[] = $row;
  }
  $variables = array(
    'header' => $header,
    'rows' => $rows,
    'attributes' => array(),
    'caption' => '',
    'colgroups' => array(),
    'sticky' => FALSE,
    'empty' => t('No tariffs have been set.'),
  );
  $table = theme_table($variables);
  return $table;
}

// TODO: Remove debug code below.
/**
 * Debug page form. Allows manually calling
 * of API.
 */
function invideous_debug_form() {
  $form = array();

  $form['intro'] = array(
    '#prefix' => '<div class="invideous-debug">',
    '#suffix' => '</div>',
    '#markup' => t('This screen allows you to manually test the invideous service api.'),
  );
  $form['path'] = array(
    '#type' => 'textfield',
    '#title' => t('API Path'),
    '#description' => t('Path to the Invideous API call.'),
    '#required' => FALSE,
  );
  $form['method'] = array(
    '#type' => 'select',
    '#title' => t('The HTTP method for querying the Invideous system.'),
    '#options' => array(
      'GET' => 'GET',
      'POST' => 'POST',
    ),
    '#default_value' => 'POST',
  );
  $form['arguments'] = array(
    '#type' => 'textarea',
    '#title' => t('Arguments'),
    '#description' => t("Parses a single string in the format key=>value,key2=>value2 into api call parameters."),
    '#default_value' => '',
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Invideous Response Type'),
    '#options' => array(
      'json' => 'json',
      'xml' => 'xml',
      'debug' => 'debug',
    ),
    '#default_value' => 'json',
  );
  $form['api_type'] = array(
    '#type' => 'select',
    '#title' => t('Invideous API Call Type'),
    '#options' => array(
      'read' => 'read',
      'write' => 'write',
      'plugin' => 'plugin',
    ),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

// TODO: Remove debug code below.
/**
 * Submit handler for the debug form. Processes
 * the arguments and settings for the API call.
 */
function invideous_debug_form_submit($form, &$form_state) {
  $settings = $form_state['values'];
  $service_path = $settings['path'];
  $service_method = $settings['method'];
  $service_arguments = $settings['arguments'];
  $service_type = $settings['type'];
  $service_api_type = $settings['api_type'];

  // Start pulling apart the textarea.
  if (!empty($service_arguments)) {
    $flat_array = explode(',', $service_arguments);

    // Break down the text field into an array.
    $processed_args = array();
    foreach ($flat_array as $parameter) {
      $split_parameter = explode('=>', $parameter);
      $split_parameter[0] = str_replace('\x98', '', $split_parameter[0]);
      $key = $split_parameter[0];
      $value = $split_parameter[1];
      $processed_args[$key] = $value;
    }
  }
  $data = (!empty($processed_args)) ? $processed_args : array();
  $invideous_service = new InvideousService();

  // Calling the request handlers directly will not work in the contrib release.
  // These will be changed to protected functions.
  $response = $invideous_service->SubmitRequest($service_path, $service_method, $data, $service_type, $service_api_type);
  $response = $invideous_service->ParseServiceResponse($response, $service_path, $data, $service_type);

  // Spit out the results.
  if (module_exists('devel')) {
    dsm($response);
  }
  else {
    drupal_set_message('<pre>' . print_r($response, TRUE) . '</pre>');
  }
}
